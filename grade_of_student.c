#include <stdio.h>
int main()
{
   int a,b,c,marks,grade=5;
   printf("Enter the marks of the student\n");
   scanf("%d",&marks);
   if(marks<100 && marks>90)
      grade =1;
   if(marks<89 && marks>70)
      grade =2;
   if(marks<69 && marks>60)
      grade =3;
   if(marks<59 && marks>40)
      grade =4;

   switch (grade)
   {
      case 1: 
         printf("GRADE A\n");
         break;

      case 2: 
         printf("GRADE B\n");
         break;

      case 3: 
         printf("GRADE C\n");
         break;

      case 4: 
         printf("GRADE D\n");
         break;

      default:
         printf("FAIL\n");
         break;
   }
   return 0;
}