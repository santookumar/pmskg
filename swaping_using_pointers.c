#include<stdio.h>
int main()
{
    int a,b,t;
    printf("Enter two numbers to swap\n");
    scanf("%d%d",&a,&b);
    printf("Numbers before swap \na=%d and b=%d\n",a,b);
    int *x,*y;
    x=&a;
    y=&b;

    t=*x;
    *x=*y;
    *y=t;
    printf("Numbers after swap \na=%d and b=%d\n",a,b);
    return 0;
}
