#include<stdio.h>
struct dob
{
    int date,month,year;
};
struct Employee
{
    char name[30];
    int id;
    float sal;
    struct dob d;
};
int main()
{
    struct Employee e;
    printf("Enter name of employee:");
    gets(e.name);

    printf("\nEnter id:");
    scanf("%d",&e.id);

    printf("\nEnter dob as DD MM YYYY:");
    scanf("%d%d%d",&e.d.date,&e.d.month,&e.d.year);

    printf("\nEnter salary:");
    scanf("%f",&e.sal);

    printf("\nEMPLOYEE PROFILE:- \n NAME-");
    puts(e.name);
    printf(" ID-%d\n DOB-%d/%d/%d\n SALARY-%f\n",e.id,e.d.date,e.d.month,e.d.year,e.sal);
    return 0;
}
